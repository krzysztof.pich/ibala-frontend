import React, {Component} from 'react'
import {Route, Routes} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import Header from "./components/global/Header";
import {Grid} from "@mui/material";
import SideMenu from "./components/global/SideMenu";
import Login from "./components/auth/login";

class App extends Component {
    render() {
        return (
            <React.Fragment>
                    <ToastContainer/>
                    <Header />
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            <SideMenu/>
                        </Grid>
                    </Grid>

            </React.Fragment>
        );
    }
}

export default App;
