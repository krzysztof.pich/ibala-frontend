import React, {Component} from "react";
import { useForm } from "react-hook-form";
import {TextField, Button, Box, Container, FormControl, InputLabel, Grid} from "@mui/material";

class Login extends Component{

    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '25ch' },
                }}>
                    <p>Login to IBALA</p>
                    <FormControl>
                        <TextField
                            required
                            id="email"
                            label="Email address"
                        />
                    </FormControl>
                    <FormControl>
                        <TextField
                            required
                            id="outlined-required"
                            label="Password"
                            type="password"
                        />
                    </FormControl>
                    <Button variant="outlined">Submit</Button>
            </Grid>

        )
    }
}

export default Login;