import React from 'react';
import { NavLink } from 'react-router-dom';
import {AppBar, Box, Button, IconButton, Toolbar, Typography} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';


const Header = () => {
    return (
        <header>
            <Box sx={{ flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                            IBALA
                        </Typography>
                        <Button color="inherit">Login</Button>
                    </Toolbar>
                </AppBar>
            </Box>
            <h1>IBALA</h1>
            <div className="links">
                <NavLink to="/" className="link" activeClassName="active" exact>IBALA</NavLink>

            </div>
        </header>
    )
}

export default Header;
