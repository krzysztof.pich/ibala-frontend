import React from 'react';
import {List, ListItemButton, ListItemIcon, ListItemText, ListSubheader} from "@mui/material";
import SendIcon from '@mui/icons-material/Send';


const SideMenu = () => {
    return (
        <List
            sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
            component="nav"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Nested List Items
                </ListSubheader>
            }>
            <ListItemButton>
                <ListItemIcon>
                    <SendIcon />
                </ListItemIcon>
                <ListItemText primary="Sent mail" />
            </ListItemButton>
        </List>
    );
}

export default SideMenu;