import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {Router, Route } from "react-router";
import {BrowserRouter, Routes} from "react-router-dom";
import Login from "./components/auth/login";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
        <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/" element={<App />} />
        </Routes>
    </BrowserRouter>
);
